package com.grobikon.testpush;

/**
 * @author Grigoriy Obraztsov on 19.06.2018.
 */
public class ConstantsVersion {
    public enum Type {
        ALPHA, PRODUCT;
    }

    public static final Type TYPE = Type.ALPHA;
}
