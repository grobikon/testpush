package com.grobikon.testpush.ui.activity;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.grobikon.testpush.R;
import com.grobikon.testpush.ui.fragment.AppsFragment;
import com.grobikon.testpush.ui.fragment.SavePushFragment;
import com.grobikon.testpush.ui.fragment.PushFragment;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class TestPushActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContent(new PushFragment());
        navigation.setSelectedItemId(R.id.navigation_random);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected int getMainContentLayout() {
        return 0;
    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_apps:
                setContent(new AppsFragment());
                return true;
            case R.id.navigation_random:
                setContent(new PushFragment());
                return true;
            case R.id.navigation_like:
                setContent(new SavePushFragment());
                return true;
        }
        return false;
    };
}
