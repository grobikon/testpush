package com.grobikon.testpush.ui.view.holder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.grobikon.testpush.R;
import com.grobikon.testpush.model.view.AppItemBodyViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AppItemBodyHolder extends BaseViewHolder<AppItemBodyViewModel> {
    @BindView(R.id.tv_text_name)
    TextView tvText;
    @BindView(R.id.iv_icon)
    CircleImageView ivIcon;

    public AppItemBodyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bindViewHolder(AppItemBodyViewModel item) {
        Context mCtx = itemView.getContext();
        tvText.setText(item.getNameApp());
        ivIcon.setImageResource(item.getImageRes());
        itemView.setOnClickListener(v -> mCtx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.getPathApp()))));
    }

    @Override
    public void unbindViewHolder() {
        tvText.setText(null);
    }
}
