package com.grobikon.testpush.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grobikon.testpush.androidX.MvpAppCompatFragment;
import com.grobikon.testpush.ui.activity.BaseActivity;

import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;

public abstract class BaseFragment extends MvpAppCompatFragment {

    @LayoutRes
    protected abstract int getMainContentLayout();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(getMainContentLayout(), container, false);
    }

    public String createToolbarTitle(final Context context) {
        return context.getString(onCreateToolbarTitle());
    }

    @StringRes
    public abstract int onCreateToolbarTitle();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public boolean needFab() {
        return false;
    }

    public void updateData() {

    }
}