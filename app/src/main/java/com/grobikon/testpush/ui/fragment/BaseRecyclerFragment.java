package com.grobikon.testpush.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.grobikon.testpush.R;
import com.grobikon.testpush.common.BaseAdapter;
import com.grobikon.testpush.common.manager.MyLinearLayoutManager;
import com.grobikon.testpush.model.view.BaseViewModel;
import com.grobikon.testpush.mvp.view.BaseView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public abstract class BaseRecyclerFragment extends BaseFragment implements BaseView {

    @BindView(R.id.rv_list)
    RecyclerView mRecyclerView;
    BaseAdapter mAdapter;

    @BindView(R.id.swipe_refresh)
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected ProgressBar mProgressBar;

    // если она true, присваиваем recyclerView scroll listener,
    // для того чтобы работала пагинация и загружались новые элементы в список.
    // В некоторый списках(инфо, открытая запись, открытый комментарий) мы не будем использовать
    // пагинацию. Для этого в их фрагментах будет присваивать этой переменной значение false
    private boolean isWithEndlessList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isWithEndlessList = true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setUpRecyclerView();
        setUpAdapter(mRecyclerView);
        setUpSwipeToRefreshLayout();

    }

    public void initButterKnife(@NonNull View view) {
        ButterKnife.bind(this, view);
    }

    public void setUpRecyclerView() {
        MyLinearLayoutManager mLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        if (isWithEndlessList) {
            // «слушает» список и оповещает, когда он скроллится
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (mLinearLayoutManager.isOnNextPagePosition()) {
                    }
                }
            });
        }

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }


    public void setUpAdapter(final RecyclerView rv) {
        mAdapter = new BaseAdapter();
        rv.setAdapter(mAdapter);
    }

    public void setUpSwipeToRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {});

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mProgressBar = getBaseActivity().getProgressBar();
    }

    @Override
    public void showRefreshing() {

    }

    @Override
    public void hideRefreshing() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showListProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideListProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getBaseActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setItems(List<BaseViewModel> items, boolean checkFirst) {
        mAdapter.setItems(items, checkFirst);
    }

    @Override
    public void addItems(List<BaseViewModel> items) {
        mAdapter.addItems(items);
    }

    @Override
    public void showAd() {

    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_base_feed;
    }

}
