package com.grobikon.testpush.ui.fragment;

import android.os.Bundle;
import android.view.View;

import com.grobikon.testpush.R;
import com.grobikon.testpush.common.manager.MyLinearLayoutManager;
import com.grobikon.testpush.model.App;
import com.grobikon.testpush.model.view.AppItemBodyViewModel;
import com.grobikon.testpush.model.view.BaseViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.SimpleItemAnimator;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class AppsFragment extends BaseRecyclerFragment{
    private String[] nameApps;
    private String[] pathApps;
    private List<App> apps;

    public AppsFragment() {
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.af_title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initButterKnife(view);
        setUpRecyclerView();
        setUpAdapter(mRecyclerView);
        setUpSwipeToRefreshLayout();

        readData();
    }

    private void readData() {
        showListProgress();
        initArray();
        setItems(parsePojoModel(apps), false);
        hideListProgress();
        hideRefreshing();
    }

    private void initArray() {
        this.nameApps = this.getResources().getStringArray(R.array.apps_name);
        this.pathApps = this.getResources().getStringArray(R.array.apps_path);
        apps = new ArrayList<>();
        List<String> nameApp = Arrays.asList(nameApps);
        int i = 0;
        for(String n : nameApp) {
            App app = new App();
            app.setNameApp(n);
            app.setPathApp(pathApps[i]);
            switch (i) {
                case 0:
                    app.setIconRes(R.drawable.jokes_1);
                    break;
                case 1:
                    app.setIconRes(R.drawable.horizontal_bar_2);
                    break;
                case 2:
                    app.setIconRes(R.drawable.biker_3);
                    break;
                case 3:
                    app.setIconRes(R.drawable.aircraft_4);
                    break;
            }
            i++;
            apps.add(app);
        }
    }

    @Override
    public void setUpRecyclerView() {
        MyLinearLayoutManager mLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private List<BaseViewModel> parsePojoModel(List<App> apps) {
        List<BaseViewModel> baseItem = new ArrayList<>();
        for (App a : apps) {
            App app = new App();
            app.setNameApp(a.getNameApp());
            app.setPathApp(a.getPathApp());
            app.setIconRes(a.getIconRes());
            baseItem.add(new AppItemBodyViewModel(app, mAdapter));
        }
        return baseItem;
    }

    @Override
    public void setUpSwipeToRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(this::readData);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mProgressBar = getBaseActivity().getProgressBar();
    }
}
