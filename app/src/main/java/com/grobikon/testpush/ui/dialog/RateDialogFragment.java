package com.grobikon.testpush.ui.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.grobikon.testpush.R;
import com.grobikon.testpush.common.SessionManager;


/**
 * @author Grigoriy Obraztsov on 25.10.2018.
 */
public class RateDialogFragment {

    public static void showDialog(final Context mCtx,
                                  final SessionManager sm) {
        View dialogView = View.inflate(mCtx, R.layout.dialog_rate, null);
        Button btn = dialogView.findViewById(R.id.btn_dialog_rate);
        ImageView iv_star1 = dialogView.findViewById(R.id.iv_star_1);
        ImageView iv_star2 = dialogView.findViewById(R.id.iv_star_2);
        ImageView iv_star3 = dialogView.findViewById(R.id.iv_star_3);
        ImageView iv_star4 = dialogView.findViewById(R.id.iv_star_4);
        ImageView iv_star5 = dialogView.findViewById(R.id.iv_star_5);

        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        iv_star1.setOnClickListener(view -> {
            dialog.dismiss();
            //сохраняем false и больше не показываем диалог оценить приложение
            sm.saveRate(false);
        });
        iv_star2.setOnClickListener(view -> {
            dialog.dismiss();
            //сохраняем false и больше не показываем диалог оценить приложение
            sm.saveRate(false);
        });
        iv_star3.setOnClickListener(view -> {
            dialog.dismiss();
            //сохраняем false и больше не показываем диалог оценить приложение
            sm.saveRate(false);
            Uri uri = Uri.parse(mCtx.getString(R.string.share_url));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            mCtx.startActivity(intent);
        });
        iv_star4.setOnClickListener(view -> {
            dialog.dismiss();
            //сохраняем false и больше не показываем диалог оценить приложение
            sm.saveRate(false);
            Uri uri = Uri.parse(mCtx.getString(R.string.share_url));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            mCtx.startActivity(intent);
        });
        iv_star5.setOnClickListener(view -> {
            dialog.dismiss();
            //сохраняем false и больше не показываем диалог оценить приложение
            sm.saveRate(false);
            Uri uri = Uri.parse(mCtx.getString(R.string.share_url));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            mCtx.startActivity(intent);
        });

        btn.setOnClickListener(v -> {
            dialog.dismiss();
            sm.toZeroRate();
        });

        dialog.show();
    }
}
