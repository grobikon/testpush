package com.grobikon.testpush.ui.view.holder;

import android.view.View;

import com.grobikon.testpush.common.BaseAdapter;
import com.grobikon.testpush.model.view.BaseViewModel;

import androidx.recyclerview.widget.RecyclerView;


/**
 * @author Grigoriy Obraztsov on 09.10.2018.
 */
//Будем наследоваться от этого класса для создания конкретных view holder’ов
public abstract class BaseViewHolder<Item extends BaseViewModel> extends RecyclerView.ViewHolder {
    private BaseAdapter baseAdapter;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    //Будет использоваться для заполнения макета данными с модели item
    public abstract void bindViewHolder(Item item);

    //Нужен для того чтобы разгружать макет, когда он не используется
    public abstract void unbindViewHolder();

    public void updateData() {
        baseAdapter.notifyDataSetChanged();
    }


    public void setBaseAdapter(BaseAdapter baseAdapter) {
        this.baseAdapter = baseAdapter;
    }
}
