package com.grobikon.testpush.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.grobikon.testpush.R;
import com.grobikon.testpush.common.NetworkManager;
import com.grobikon.testpush.common.SessionManager;
import com.grobikon.testpush.di.TestPushApp;
import com.grobikon.testpush.model.PushModel;
import com.grobikon.testpush.model.view.BaseViewModel;
import com.grobikon.testpush.rest.api.PushApi;
import com.grobikon.testpush.rest.model.request.TestPushModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class PushFragment extends BaseFragment {
    @BindView(R.id.et_token)
    EditText etToken;
    @BindView(R.id.et_push)
    EditText etPush;
    @BindView(R.id.et_user)
    EditText etUser;
    @Inject
    SessionManager sm;
    @Inject
    NetworkManager mNetworkManager;
    @Inject
    PushApi pushApi;


    public PushFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestPushApp.getApplicationComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.b_send_push)
    public void clickBGenSwear() {
        if ("".equals(etToken.getText().toString()) || "".equals(etPush.getText().toString()) || "".equals(etUser.getText().toString())){
            showMessage(getString(R.string.et_push_error));
        } else {
            sendMessage(etToken.getText().toString(), etUser.getText().toString(), etPush.getText().toString());
        }
    }

    private int getNextKey() {
        return getBaseActivity().getRealm().where(PushModel.class).count() == 0 ? 0 : (getBaseActivity().getRealm().where(PushModel.class).max("id")).intValue() + 1;
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragmnet_push;
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.sf_title;
    }

    private void sendMessage(final String token, final String apiUser, final String mess) {
        mNetworkManager.getNetworkObservable()
                .flatMap(aBoolean -> aBoolean
                        ? onCreateLoadDataObservable(token, apiUser, mess)
                        : onCreateRestoreDateObservable())
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> onLoadStart())
                .doFinally(() -> onLoadingFinish())
                .subscribe(
                        repositories -> onLoadingSuccess(),
                        error -> {
                            error.printStackTrace();
                            onLoadingFailed(error);
                        });
    }

    private ObservableSource<?> onCreateRestoreDateObservable() {
        return Observable
                .fromCallable(null)
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnNext(__ -> Toast.makeText(getBaseActivity(), "У вас отключен \"Интернет\".", Toast.LENGTH_SHORT).show())
                .observeOn(Schedulers.io());
    }

    private Observable<BaseViewModel> onCreateLoadDataObservable(final String token,
                                                                 final String apiUser,
                                                                 final String mess) {
        return pushApi.sentPush(new TestPushModel(token, apiUser, mess).toMap())
                .flatMap(push -> {
                    List<BaseViewModel> baseItems = new ArrayList<>();
                    return Observable.fromIterable(baseItems);
                });
    }

    private void onLoadStart() {
        getBaseActivity().getProgressBar().setVisibility(View.VISIBLE);
    }

    private void onLoadingFinish(){
        getBaseActivity().getProgressBar().setVisibility(View.GONE);
    }

    public void onLoadingSuccess(){
        saveToDb(etUser.getText().toString(), etPush.getText().toString());
        etToken.setText("");
        etPush.setText("");
        etUser.setText("");
        showMessage(getString(R.string.send_success));
    }

    public void onLoadingFailed(Throwable error){
        Toast.makeText(getBaseActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void showMessage(final String mess) {
        Toast.makeText(getBaseActivity(), mess, Toast.LENGTH_SHORT).show();
    }

    // Мы будем вызывать этот метод тогда, когда захотим сохранить данные в DB
    public void saveToDb(final String apiUser, final String mess) {
        if (!getBaseActivity().getRealm().isInTransaction()) getBaseActivity().getRealm().beginTransaction();
        PushModel nekdoPhoto = getBaseActivity().getRealm().createObject(PushModel.class, getNextKey());
        nekdoPhoto.setApiUser(apiUser);
        nekdoPhoto.setMessage(mess);
        getBaseActivity().getRealm().commitTransaction();
    }

}






































