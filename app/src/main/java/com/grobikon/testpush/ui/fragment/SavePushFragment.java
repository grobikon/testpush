package com.grobikon.testpush.ui.fragment;

import android.os.Bundle;
import android.view.View;

import com.grobikon.testpush.R;
import com.grobikon.testpush.common.manager.MyLinearLayoutManager;
import com.grobikon.testpush.di.TestPushApp;
import com.grobikon.testpush.model.PushModel;
import com.grobikon.testpush.model.view.BaseViewModel;
import com.grobikon.testpush.model.view.PushItemBodyViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.SimpleItemAnimator;
import io.realm.Realm;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class SavePushFragment extends BaseRecyclerFragment{
    @Inject
    Realm realm;

    public SavePushFragment() {
    }

    @Override
    public int onCreateToolbarTitle() {
        return R.string.lf_title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestPushApp.getApplicationComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initButterKnife(view);
        setUpRecyclerView();
        setUpAdapter(mRecyclerView);
        setUpSwipeToRefreshLayout();

        readData();
    }

    @Override
    public void setUpRecyclerView() {
        MyLinearLayoutManager mLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void readData() {
        showListProgress();
        List<PushModel> swears = realm.where(PushModel.class).findAll();
        setItems(parsePojoModel(swears), false);
        hideListProgress();
        hideRefreshing();
    }

    private List<BaseViewModel> parsePojoModel(List<PushModel> swears) {
        List<BaseViewModel> baseItem = new ArrayList<>();
        for (PushModel s : swears) {
            PushModel pushModel = new PushModel();
            pushModel.setApiUser(s.getApiUser());
            pushModel.setMessage(s.getMessage());
            baseItem.add(new PushItemBodyViewModel(pushModel, mAdapter));
        }
        return baseItem;
    }

    @Override
    public void setUpSwipeToRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(this::readData);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mProgressBar = getBaseActivity().getProgressBar();
    }
}
