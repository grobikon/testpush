package com.grobikon.testpush.ui.view.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.grobikon.testpush.R;
import com.grobikon.testpush.di.TestPushApp;
import com.grobikon.testpush.model.PushModel;
import com.grobikon.testpush.model.view.PushItemBodyViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class PushItemBodyHolder extends BaseViewHolder<PushItemBodyViewModel> {
    @BindView(R.id.tv_user)
    TextView tvUser;
    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.iv_delete)
    ImageView iv_delete;
    @Inject
    Realm realm;

    public PushItemBodyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        TestPushApp.getApplicationComponent().inject(this);
    }

    @Override
    public void bindViewHolder(PushItemBodyViewModel item) {
        Context mCtx = itemView.getContext();
        tvUser.setText(item.getApiUser());
        tvText.setText(item.getMess());

        iv_delete.setOnClickListener(view -> {
            if (!realm.isInTransaction()) realm.beginTransaction();
            PushModel swear = realm.where(PushModel.class).equalTo("id", item.getId()).findFirst();
            if (swear != null) {
                RealmResults<PushModel> rows = realm.where(PushModel.class).equalTo("id", swear.getId()).findAll();
                rows.deleteAllFromRealm();
            }

            realm.commitTransaction();
            int pos = getAdapterPosition();
            item.getAdapter().removeItem(pos);
        });
    }

    @Override
    public void unbindViewHolder() {
        tvUser.setText(null);
        tvText.setText(null);
    }
}
