package com.grobikon.testpush.rest;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    public static final String UMORILI_BASE_URL = "https://api.pushover.net/1/";
    private Retrofit mRetrofit;
    private OkHttpClient okHttpClientLogging = new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)).build();


    public RestClient() {
        mRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(UMORILI_BASE_URL)
                .client(okHttpClientLogging)
                .build();
    }

    //переменные для инициализациии RestApi сервисов
    public <S> S createService(Class<S> serviceClass) {
        return mRetrofit.create(serviceClass);
    }
}
