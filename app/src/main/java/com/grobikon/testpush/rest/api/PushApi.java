package com.grobikon.testpush.rest.api;

import com.grobikon.testpush.rest.model.response.Status;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface PushApi {

    @POST(ApiMethods.SENT_PUSH)
    Observable<Status> sentPush(@QueryMap Map<String, String> map);
}
