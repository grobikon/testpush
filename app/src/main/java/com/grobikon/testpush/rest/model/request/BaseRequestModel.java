package com.grobikon.testpush.rest.model.request;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseRequestModel {

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();

        onMapCreate(map);

        return map;
    }

    //чтобы передовать массив map в дочернии классы
    protected abstract void onMapCreate(Map<String, String> map);
}
