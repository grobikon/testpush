package com.grobikon.testpush.rest.model.request;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestPushModel extends BaseRequestModel{
    private String token;
    private String user;
    private String message;

    public TestPushModel(String token, String user, String message) {
        this.token = token;
        this.user = user;
        this.message = message;
    }


    @Override
    protected void onMapCreate(Map<String, String> map) {
        map.put("token", token);
        map.put("user", user);
        map.put("message", message);
    }
}
