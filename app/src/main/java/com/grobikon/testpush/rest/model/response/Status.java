package com.grobikon.testpush.rest.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Status {

    private int status;
    private String request;
}
