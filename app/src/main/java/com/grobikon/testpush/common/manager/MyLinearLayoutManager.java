package com.grobikon.testpush.common.manager;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * @author Grigoriy Obraztsov.
 */

// Этот класс имеет доступ к вью списка и будет проверять,
// находится ли список в той позиции, в которой нам нужно подгружать следующие элементы
public class MyLinearLayoutManager extends LinearLayoutManager {

    public MyLinearLayoutManager(Context context) {
        super(context);
    }

    public MyLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public MyLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public boolean isOnNextPagePosition() {
        int visibleItemCount = getChildCount();
        int totalItemCount = getItemCount();
        int pastVisibleItems = findFirstVisibleItemPosition();

        int endItemCount = totalItemCount > 10 ? totalItemCount - 3 : totalItemCount;

        return endItemCount != 0 && (visibleItemCount + pastVisibleItems) >= endItemCount;
    }
}
