package com.grobikon.testpush.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.grobikon.testpush.di.TestPushApp;
import com.grobikon.testpush.rest.RestClient;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.realm.internal.Util;

/**
 * @author Grigoriy Obraztsov on 08.10.2018.
 */
// С помощью этого класса будем проверять,
// имеет ли устройство доступ в интернет и доступен ли сервер http://umorili.herokuapp.com api
public class NetworkManager {

    @Inject
    Context mContext;

    private static final String TAG = NetworkManager.class.getSimpleName();

    public NetworkManager() {
        TestPushApp.getApplicationComponent().inject(this);
    }

    public boolean isOnline() {
        ConnectivityManager cm = ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE));
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return ((networkInfo != null && networkInfo.isConnected()) || Util.isEmulator());
    }

    public Callable<Boolean> isUmoriliReachableCallable() {
        return () -> {
            try {
                if (!isOnline()) {
                    return false;
                }

                URL url = new URL(RestClient.UMORILI_BASE_URL);
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(2000);
                urlc.connect();

                return true;
            } catch (Exception e) {
                return false;
            }
        };
    }

    public Observable<Boolean> getNetworkObservable(){
        return Observable.fromCallable(isUmoriliReachableCallable());
    }
}
