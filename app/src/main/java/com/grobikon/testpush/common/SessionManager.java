package com.grobikon.testpush.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Grigoriy Obraztsov.
 */

public class SessionManager {

    /**
     * SharedPreferences — постоянное хранилище на платформе Android, используемое приложениями для
     * хранения своих настроек, например. Это хранилище является относительно постоянным, пользователь
     * может зайти в настройки приложения и очистить данные приложения, тем самым очистив все данные в хранилище.
     */

    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    public static final String STORAGE_NAME = "PushModel";
    private static final String COUNT_LOAD = "count_load";
    private static final String RATE_APP = "rate";
    private static final String RATE_APP_COUNT = "rate_app_count";

    /**
     * Для работы с данными постоянного хранилища нам понадобится экземпляр класса SharedPreferences,
     * который можно получить у любого объекта, унаследованного от класса android.content.Context
     * (например, Activity или Service). У объектов этих классов (унаследованных от Context) есть
     * метод getSharedPreferences, который принимает 2 параметра:
     * name — выбранный файл настроек. Если файл настроек с таким именем не существует, он будет создан при вызове метода edit() и фиксировании изменений с помощью метода commit().
     * mode — режим работы. Возможные значения:
     */

    public SessionManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        //MODE_PRIVATE — используется в большинстве случаев для приватного доступа к данным приложением-владельцем
        //sharedPreferences = ctx.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public SessionManager(Context ctx) {
        //MODE_PRIVATE — используется в большинстве случаев для приватного доступа к данным приложением-владельцем
        sharedPreferences = ctx.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    //Сохраняем значение кол-ва загрузок анеедотов
    public void saveCountLoad(int countLoad){
        editor.remove(COUNT_LOAD);
        editor.putInt(COUNT_LOAD, countLoad);
        editor.apply();
    }

    //Получаем значение кол-ва загрузок анеедотов
    public int getCountLoad(){
        return sharedPreferences.getInt(COUNT_LOAD, 0);
    }

    // если показали диалог то false
    public void saveRate(boolean rate) {
        editor.putBoolean(RATE_APP, rate);
        editor.apply();
    }

    // получаем значение false - тогда надо показывать диалог
    public boolean isRate() {
        return sharedPreferences.getBoolean(RATE_APP, true);
    }

    // сохраняем кол-во запусков приложения
    public void saveCountStarts() {
        if (getCountStarts() >= 3) editor.putInt(RATE_APP_COUNT, 0);
        else editor.putInt(RATE_APP_COUNT, getCountStarts() + 1);
        editor.apply();
    }

    // получаем кол-во запусков
    public int getCountStarts() {
        return sharedPreferences.getInt(RATE_APP_COUNT, 0);
    }

    // обнуляем счетчики для показа оценки
    public void toZeroRate() {
        editor.putInt(RATE_APP_COUNT, 0);
        editor.apply();
    }
}
