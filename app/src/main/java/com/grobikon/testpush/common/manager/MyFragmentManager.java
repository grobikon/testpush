package com.grobikon.testpush.common.manager;

import com.grobikon.testpush.ui.activity.BaseActivity;
import com.grobikon.testpush.ui.fragment.BaseFragment;

import java.util.Stack;

import androidx.annotation.IdRes;
import androidx.fragment.app.FragmentTransaction;

/**
 * @author Grigoriy Obraztsov.
 */

public class MyFragmentManager {

    private static final int EMPTY_FRAGMENT_STACK_SIZE = 1;

    private Stack<BaseFragment> mFragmentStack = new Stack<>();

    private BaseFragment mCurrentFragment;

    public void setFragment(final BaseActivity activity, final BaseFragment fragment, @IdRes int containerId){

        if (activity != null && !activity.isFinishing() && !isAlreadyContains(fragment)){
            FragmentTransaction fragmentTransaction = createAddTransaction(activity, fragment, true);
            fragmentTransaction.replace(containerId, fragment);
            commitAddTransaction(activity, fragment, fragmentTransaction, false);
        }
    }

    public void addFragment(final BaseActivity activity, final BaseFragment fragment, @IdRes int containerId){
        if (activity != null && !activity.isFinishing() && !isAlreadyContains(fragment)){
            FragmentTransaction fragmentTransaction = createAddTransaction(activity, fragment, true);
            fragmentTransaction.add(containerId, fragment);
            commitAddTransaction(activity, fragment, fragmentTransaction, true);
        }
    }

    public boolean removeCurrentFragment(BaseActivity activity){
        return removeFragment(activity, mCurrentFragment);
    }

    public boolean removeAllFragments(BaseActivity activity){
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        while (mFragmentStack.size() > 0){
            transaction.remove(mCurrentFragment); //Удаляет фрагмент из активности
            commitTransaction(activity, transaction); //Совершать транзакцию
            mFragmentStack.pop();
            if (mFragmentStack.empty()) {
                mCurrentFragment = null;
                return true;
            }
            mCurrentFragment = mFragmentStack.lastElement(); // присваиваем текущий фрагмент из стека берем последний
        }

        return false;
    }

    public boolean removeFragment(BaseActivity activity, BaseFragment fragment){
        boolean canRemove = fragment != null && mFragmentStack.size() > EMPTY_FRAGMENT_STACK_SIZE;

        if (canRemove) {
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

            fragment.setUserVisibleHint(false);
            mFragmentStack.pop();
            mCurrentFragment = mFragmentStack.lastElement();
            mCurrentFragment.setUserVisibleHint(true);

            transaction.remove(fragment); //Удаляет фрагмент из активности
            commitTransaction(activity, transaction); //Совершать транзакцию
        }
        return canRemove;
    }

    private FragmentTransaction createAddTransaction(final BaseActivity activity,
                                                     final BaseFragment fragment,
                                                     final boolean addToBackStack){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getTag());
        }
        return fragmentTransaction;
    }

    private void commitAddTransaction(final BaseActivity activity,
                                      final BaseFragment fragment,
                                      final FragmentTransaction transaction,
                                      final boolean addToBackStack) {
        if (transaction != null) {
            mCurrentFragment = fragment;
            if (!addToBackStack) {
                mFragmentStack = new Stack<>();
            }
            mFragmentStack.add(fragment);
            commitTransaction(activity,  transaction);
        }
    }

    private void commitTransaction(BaseActivity activity,
                                   FragmentTransaction transaction) {
        transaction.commit();
        activity.fragmentOnScreen(mCurrentFragment);
    }

    public boolean isAlreadyContains(final BaseFragment fragment) {
        if (fragment == null) {
            return false;
        }
        return mCurrentFragment != null && mCurrentFragment.getClass().getName().equals(fragment.getClass().getName());
    }

    public Stack<BaseFragment> getFragmentStack() {
        return mFragmentStack;
    }

    public BaseFragment getCurrentFragment() {
        return mFragmentStack.lastElement();
    }

    public void updateDataInFragment() {
        if (mFragmentStack.size() != 0) {
            getCurrentFragment().updateData();
        }
    }
}
