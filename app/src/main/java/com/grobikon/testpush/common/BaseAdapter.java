package com.grobikon.testpush.common;

import android.util.ArrayMap;
import android.view.ViewGroup;

import com.grobikon.testpush.model.view.BaseViewModel;
import com.grobikon.testpush.ui.view.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @author Grigoriy Obraztsov on 09.10.2018.
 */
public class BaseAdapter extends RecyclerView.Adapter<BaseViewHolder<BaseViewModel>> {

    private List<BaseViewModel> list = new ArrayList<>(); //содержит все добавленные в адаптер элементы Отсюда берутся данные для заполнения макета в методе onBindViewHolder().

    private ArrayMap<Integer, BaseViewModel> mTypeInstances = new ArrayMap<>(); // список из элементов ключ-значение, где ключ это тип модели и макета,
    // а значение — сама модель. Нужен для того чтобы создавать view holder
    // конкретного типа в методе onCreateViewHolder().

    @Override
    public BaseViewHolder<BaseViewModel> onCreateViewHolder(ViewGroup parent, int viewType) {
        // Вызывается метод onCreateViewHolder() с параметром int viewType, взятым из метода
        // getItemViewType(int position) (мы переопределяем метод getItemViewType(), возвращая тип модели).
        // Дальше в методе onCreateViewHolder() получаем необходимый тип модели и вызываем у нее
        // метод createViewHolder(). После этого вызывается onBindViewHolder(), где заполняется макет.
        return mTypeInstances.get(viewType).createViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder<BaseViewModel> holder, int position) {
        holder.setBaseAdapter(this);
        holder.bindViewHolder(getItem(position));
    }

    @Override
    public void onViewRecycled(BaseViewHolder<BaseViewModel> holder) {
        // При прокрутке адаптер освобождает ненужные view holder’ы и вызывается метод
        // onViewRecycled(), где мы очищаем view holder. Новый view holder создается
        // если нет свободных такого же типа.
        super.onViewRecycled(holder);
        holder.unbindViewHolder();
    }

    public void registerTypeInstance(final BaseViewModel item) {
        if (!mTypeInstances.containsKey(item.getType())) {
            mTypeInstances.put(item.getType().getValue(), item);
        }
    }

    public void setItems(final List<BaseViewModel> items, boolean checkFirst) {
        if (checkFirst && !items.isEmpty()) return;
        clearList();
        addItems(items);
    }

    //Добавляем элементы в адаптер методом
    public void addItems(final List<? extends BaseViewModel> newItems) { //? extends BaseViewModel - это означет что можно передовать классы наследники BaseViewModel
        for (BaseViewModel newItem : newItems) { //добавляем элементы в список моделей list
            registerTypeInstance(newItem);       // и в список типов mTypeInstances(если там уже нет такого типа)
        }
        list.addAll(newItems);
        notifyDataSetChanged();
    }

    public void clearList() {
        list.clear();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getType().getValue();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public BaseViewModel getItem(int position) {
        return list.get(position);
    }

    public List<BaseViewModel> getItems() {
        return list;
    }


    // Он будет перебирать все элементы списка, проверять является ли элемент
    // реальным и возвращать реальное количество элементов.
    public int getRealItemCount() {
        int count = 0;
        for (int i = 0; i < getItemCount(); i++) {
            if (!getItem(i).isItemDecorator()) {
                count += 1;
            }
        }
        return count;
    }

    public void removeItem(int pos){
        list.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, list.size());
    }

}
