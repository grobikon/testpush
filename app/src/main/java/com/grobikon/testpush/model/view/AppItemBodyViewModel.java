package com.grobikon.testpush.model.view;

import android.view.View;

import com.grobikon.testpush.common.BaseAdapter;
import com.grobikon.testpush.model.App;
import com.grobikon.testpush.ui.view.holder.AppItemBodyHolder;
import com.grobikon.testpush.ui.view.holder.BaseViewHolder;

public class AppItemBodyViewModel extends BaseViewModel{
    private String nameApp;
    private String pathApp;
    private int imageRes;
    private BaseAdapter mAdapter;

    public AppItemBodyViewModel(App app, BaseAdapter mAdapter) {
        this.nameApp = app.getNameApp();
        this.pathApp = app.getPathApp();
        this.imageRes = app.getIconRes();
        this.mAdapter = mAdapter;
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.AppBody;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new AppItemBodyHolder(view);
    }

    @Override
    public boolean isItemDecorator() {
        return true;
    }

    public String getNameApp() {
        return nameApp;
    }

    public BaseAdapter getAdapter() {
        return mAdapter;
    }

    public String getPathApp() {
        return pathApp;
    }

    public int getImageRes() {
        return imageRes;
    }
}
