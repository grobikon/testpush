package com.grobikon.testpush.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */

@Setter
@Getter
public class PushModel extends RealmObject {
    @PrimaryKey()
    @Expose(deserialize = false, serialize = false)
    private int id;
    private String apiUser;
    private String message;

    public PushModel() {
    }
}
