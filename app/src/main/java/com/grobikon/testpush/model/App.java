package com.grobikon.testpush.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class App {
    private String nameApp;
    private String pathApp;
    private int iconRes;

    public App() {
    }
}
