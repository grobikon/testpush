package com.grobikon.testpush.model.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grobikon.testpush.R;
import com.grobikon.testpush.ui.view.holder.BaseViewHolder;

import androidx.annotation.LayoutRes;


/**
 * @author Grigoriy Obraztsov on 09.10.2018.
 */
public abstract class BaseViewModel {

    //Для того чтобы отличать модели разного типа, а также для инфлейта необходимого макета.
    public abstract LayoutTypes getType();

    public BaseViewHolder createViewHolder(final ViewGroup parent) {
        return onCreateViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(getType().getValue(), parent, false));
    }

    //Нужен для того чтобы переложить ответственность за создание конкретного ViewHolder’а на дочерние классы
    protected abstract BaseViewHolder onCreateViewHolder(View view);

    public enum LayoutTypes {
        SwearBody(R.layout.item_push_body),
        AppBody(R.layout.item_app_body);

        private final int id;

        LayoutTypes(final int resId) {
            this.id = resId;
        }

        @LayoutRes
        public int getValue() {
            return id;
        }
    }

    // Теперь нам остается просто переопределить этот метод в тех моделях,
    // которые мы не будем считать реальными.
    public boolean isItemDecorator() {
        return false;
    }
}
