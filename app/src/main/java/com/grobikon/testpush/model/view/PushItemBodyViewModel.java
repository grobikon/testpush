package com.grobikon.testpush.model.view;

import android.view.View;

import com.grobikon.testpush.common.BaseAdapter;
import com.grobikon.testpush.model.PushModel;
import com.grobikon.testpush.ui.view.holder.BaseViewHolder;
import com.grobikon.testpush.ui.view.holder.PushItemBodyHolder;

/**
 * @author Grigoriy Obraztsov on 17.12.2018.
 */
public class PushItemBodyViewModel extends BaseViewModel{
    private String mess;
    private String apiUser;
    private BaseAdapter mAdapter;
    private int id;

    public PushItemBodyViewModel(PushModel pushModel, BaseAdapter adapter) {
        this.id = pushModel.getId();
        this.mess = pushModel.getMessage();
        this.apiUser = pushModel.getApiUser();
        this.mAdapter = adapter;
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.SwearBody;
    }

    @Override
    protected BaseViewHolder onCreateViewHolder(View view) {
        return new PushItemBodyHolder(view);
    }

    public String getMess() {
        return mess;
    }

    public String getApiUser() {
        return apiUser;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean isItemDecorator() {
        return true;
    }

    public BaseAdapter getAdapter() {
        return mAdapter;
    }
}
