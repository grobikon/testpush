package com.grobikon.testpush.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.grobikon.testpush.common.NetworkManager;
import com.grobikon.testpush.common.SessionManager;
import com.grobikon.testpush.common.manager.MyFragmentManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Grigoriy Obraztsov on 05.03.2018.
 */

@Module
public class ManagerModule {

    @Singleton
    @Provides
    MyFragmentManager provideMyFragmentManager() {
        return new MyFragmentManager();
    }


    @Singleton
    @Provides
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }


    @Singleton
    @Provides
    SessionManager sessionManager(SharedPreferences preferences){
        return new SessionManager(preferences);
    }

    @Singleton
    @Provides
    public SharedPreferences providesSharedPreference(Context context){
        return context.getSharedPreferences(SessionManager.STORAGE_NAME, Context.MODE_PRIVATE);
    }
}
