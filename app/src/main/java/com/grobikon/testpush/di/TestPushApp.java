package com.grobikon.testpush.di;

import android.app.Application;
import android.content.Context;

import com.grobikon.testpush.di.component.ApplicationComponent;
import com.grobikon.testpush.di.component.DaggerApplicationComponent;
import com.grobikon.testpush.di.module.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TestPushApp extends Application {
    private static ApplicationComponent sApplicationComponent;

    public static TestPushApp get(Context context){
        return (TestPushApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
        initRealm();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void initComponent(){
        sApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }

    public static ApplicationComponent getApplicationComponent() {
        return sApplicationComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

}
