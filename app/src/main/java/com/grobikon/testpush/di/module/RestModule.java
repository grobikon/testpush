package com.grobikon.testpush.di.module;

import com.grobikon.testpush.rest.RestClient;
import com.grobikon.testpush.rest.api.PushApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Grigoriy Obraztsov on 08.10.2018.
 */
@Module
public class RestModule {
    private RestClient mRestClient;

    public RestModule() {
        this.mRestClient = new RestClient();
    }

    @Singleton
    @Provides
    public RestClient provideRestClient(){
        return mRestClient;
    }

    @Singleton
    @Provides
    public PushApi provideGetAna(){
        return mRestClient.createService(PushApi.class);
    }
}
