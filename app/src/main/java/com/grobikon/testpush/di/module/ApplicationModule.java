package com.grobikon.testpush.di.module;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Grigoriy Obraztsov on 05.03.2018.
 */

@Module
public class ApplicationModule {
    private Application mApplication;

    public ApplicationModule(final Application application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Singleton
    @Provides
    LayoutInflater provideLayoutInflater(){
        return (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

}
