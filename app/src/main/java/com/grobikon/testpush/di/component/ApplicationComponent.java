package com.grobikon.testpush.di.component;

import com.grobikon.testpush.common.NetworkManager;
import com.grobikon.testpush.di.module.ApplicationModule;
import com.grobikon.testpush.di.module.ManagerModule;
import com.grobikon.testpush.di.module.RealmModule;
import com.grobikon.testpush.di.module.RestModule;
import com.grobikon.testpush.ui.activity.BaseActivity;
import com.grobikon.testpush.ui.activity.TestPushActivity;
import com.grobikon.testpush.ui.fragment.SavePushFragment;
import com.grobikon.testpush.ui.fragment.PushFragment;
import com.grobikon.testpush.ui.view.holder.PushItemBodyHolder;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Grigoriy Obraztsov on 05.03.2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ManagerModule.class, RestModule.class, RealmModule.class})
public interface ApplicationComponent {
    //ACTIVITY
    void inject(BaseActivity activity);
    void inject(TestPushActivity activity);

    //FRAGMENT
    void inject(SavePushFragment fragment);
    void inject(PushFragment fragment);

    //HOLDER
    void inject(PushItemBodyHolder holder);

    //managers
    void inject(NetworkManager manager);

}
