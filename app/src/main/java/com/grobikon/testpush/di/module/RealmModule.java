package com.grobikon.testpush.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author Grigoriy Obraztsov on 21.11.2018.
 */
@Module
public class RealmModule {

    @Provides
    @Singleton
    Realm provideRealm(RealmConfiguration realmConfiguration) {
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
        }catch (Exception e){
            realm = Realm.getInstance(realmConfiguration);
        }
        return realm;
    }

    @Provides
    @Singleton
    RealmConfiguration provideRealmConfiguration() {
        // Get a Realm instance for this thread
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        return config;
    }
}
